<?php

namespace App;

//Created by Mahesh

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Billing extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;
    protected $table = 'billing';
    protected $fillable = ['id', 'cust_id', 'user_id', 'status', 'created_at', 'updated_at'];

    //public function bill()
    //{
     //   return $this->hasMany('App\Bill');
      //  return $this->hasMany('App\Bill','bill_id','id');
    //}

}
