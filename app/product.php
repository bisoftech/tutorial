<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Input;
class Product extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    
    
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'quantity', 'rate', 'productexp_date', 'status', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];
    public static function func($id)
    {   $data=Input::all();
        $resp['success']='true';
        $resp['msg']='model func() is call';
        $resp['input']=$data;
        $resp['id']=$id;
        $resp['app_path']=app_path();
        $resp['base_path']=base_path();
        $resp['url']=action('ProductController@allProduct');
        //$resp['rout'] = route();
        return $resp;
    }
}
