<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Bill;
use App\Billing;
use Input;
use Auth;
use DB;

//Created by Mahesh

class BillController extends Controller
{
     public function showAll()
    {
           $resp=array();
           try
           {
                if (Auth::check()) 
                {     
                $resp['success']   = 'true';
                $resp['msg']       = "Show Bill data";
                $resp['data']      = Bill::all();
                }
                else 
                {
                $resp['success']   = 'false';
                $resp['msg']       = "Sorry you are not authorized user";    
                }
           }

           catch(Exception $ex)
           {
                $resp['success']   = 'false';
                $resp['msg']       = "Can not display Bill data";

                if(env('APP_ENV')=='local')

                $resp['ex']=$ex->getMessage();

           }
                return $resp;
    }

    public function savebill()
    {
        $resp=array();
               
        try
        {
                $billing['cust_id']        = Input::get('cust_id');
                $billing['user_id']        = Input::get('user_id');
                $billing['status']         = Input::get('status');
                $billing['created_at']     = date('Y-m-d H:i:s');
                
                $id                        = Billing::insertGetId($billing);

                $productbill['bill_id']    = $id;
                $productbill['product_id'] = Input::get('product_id');
                $productbill['quantity']   = Input::get('quantity');
                $productbill['rate']       = Input::get('rate');
                $productbill['created_at'] = date('Y-m-d H:i:s');           
 
                $resp['productbill']       = Bill::insert($productbill);

                $resp['success']=true;
                $resp['msg']='Bill Successfully Created';        
        } 
        catch(Exception $ex)
        {
                $resp['success']=false;
                $resp['msg']='Cannot create bill please try later';
                if(env('APP_ENV')=='local')
                $resp['ex']=$ex->getMessage();    
        }
            
                return $resp;
    }
        
}

