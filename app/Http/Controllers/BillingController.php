<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Billing;
use App\Bill;
use Auth;

class BillingController extends Controller
{
     public function showAll()
    {
            $resp = array();
            try
            {
              if (Auth::check())  
              {
                $resp['success']   = 'true';
                $resp['msg']       = 'Data Display Successfully';
                $resp['data']      = Billing::all();
                foreach ($resp['data'] as $bill) 
                {
                    $bill->bill;
                }
              }
              else
              {
                $resp['success']   = 'false';
                $resp['msg']       = 'User not Login';
              }
            }
            catch(Exception $ex)
            {
                $resp['success']   = false;
                $res['msg']        = 'Cannot display data';

                if(env('APP_ENV') == 'local')

                $resp['ex']        = $ex->getMessage();    
            }

            return $resp; 
    }
}
