<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Input;
use Session;

class UserController extends Controller
{
    public function showAll()
    {
            $resp = array();
            try
            {
                if (Auth::check()) 
                {
                $resp['success']    = true;
                $resp['msg']        = 'Show User Details';
                $resp['data']       = User::all();
                }
                else 
                {
                $resp['success']    = false;
                $resp['msg']        = "Sorry you are not authorized user";    
                } 
            }

            catch(Exception $ex)
            {
                $resp['success']    = false;
                $resp['msg']        = 'Cannot dislay user details';

                if(env('APP_ENV') == 'local')

                $resp['ex']         = $ex->getMessage();
            }

            return $resp;
    }

    
    public function userLogin()
    {
                   $input=Input::all();    
                    try{
                        $resp=array();
                        if(Auth::attempt(['username'=>$input['username'],'password'=>$input['password']])){
                        $resp['success']=true;
                        $resp['msg']='Login Successful';
                        $resp['user']=Auth::user();
                        Session::put('user',$resp['user']);
                        } 
                        else 
                        {
                        $resp['success']=false;
                        $resp['msg']='Invalid Credentials';
                        }
                        }
                    catch(Exception $ex){
                        $resp['success']=false;
                        $resp['msg']='Cannot login please try later';
                        if(env('APP_ENV')=='local')
                        $resp['ex']=$ex->getMessage();    
                    }
                      return $resp;
<<<<<<< HEAD
    }
    
   public function logout()
   {
=======
                }
<<<<<<< HEAD

 public function logout()
=======
            public function logout()
>>>>>>> 162dc5313b1dda610febd49f786e4d5198239aa6
            {
>>>>>>> e12e98c35d7016abcf1954d8c72ceba50bf888a1
                if(Auth::check())
                {
                    Auth::logout();
                    $resp['success']='true';
                }
                else
<<<<<<< HEAD
                        $resp['msg']='not login';
                return $resp;
            }
    
=======
                    $resp['msg']='not login';
                return $resp;
<<<<<<< HEAD
   }
=======
            }
>>>>>>> 162dc5313b1dda610febd49f786e4d5198239aa6
}
>>>>>>> e12e98c35d7016abcf1954d8c72ceba50bf888a1

}
