//<div ng-app="tutorial" ng-controller="testCtrl">
//Hello {{firstName+ " " +lastName }}


//</div>

//<script>
var app=angular.module("tutorial",['ui.router']);

app.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider) {

	$stateProvider.state('login',{
		url:"/login",
		templateUrl: 'views/login.html',
		controller: 'loginCtrl'
	});

	$stateProvider.state('home',{
		url:"/home",
		templateUrl: 'views/home.html',
		controller : 'homeCtrl'
			
		
	});

	$stateProvider.state('home.Billing',{
		url:"/Billing",
		templateUrl: 'views/Billing.html',
		controller: 'billingCtrl'

	});
	
	$stateProvider.state('home.Employee',{

		url:"/Employee",
		templateUrl: 'views/Employee.html',
		controller: 'empCtrl'

	});

	$stateProvider.state('home.productDetails',{
		url:"/productDetails",
		templateUrl: 'views/productDetails.html',
		controller: 'prodCtrl'
	
	});


}])


app.controller("loginCtrl",['$scope','$state','apiUtility','userService',function($scope,$state,apiUtility,userService)
{

	$scope.user ={};

		

	//$scope.username="test";
	//$scope.password="pwd";
	$scope.test = function()
	{

		apiUtility.executeApi('POST','http://localhost:8000/userlogin',{"username":$scope.user.username,"password":$scope.user.password})
		.then(function(response){

			console.log(response);





			//if($scope.user.username == $scope.username && $scope.user.password == $scope.password)
			if(response.success==true)
		{
			userService.setUser(response.user);
			
			$state.go('home');
			console.log();
		}
		else
		{
			/*if($scope.user.username == $scope.username)
				$scope.usernameCorrect = true;

			if($scope.user.password == $scope.password)
				$scope.passwordCorrect = true; */

			$scope.message="Enter correct user name and password";

			//$scope.message2="Enter correct password";
		}

		});

		//Compare username and password
		

	}


}]);

app.controller("homeCtrl",['$scope','$state','userService', function($scope,$state,userService){

	$scope.user=userService.getUser();

	if($scope.user==null)
	{
		$state.go('login');
	}

	

    
	$scope.loadTabView=function(tabName)
		{

			$state.go('home.'+tabName);

		}
	

}]);

app.controller("billingCtrl",['$scope','$state','apiUtility',function($scope,$state,apiUtility)
{

	apiUtility.executeApi('GET','http://localhost:8000/billing')
	.then(function(response){

		$scope.Billing=response.data;

	});


 	//$http.get("http://localhost:8000/billing").
 	//success(function(response)
 	//{
 		
 	//});


}]);


app.controller("empCtrl",['$scope','$state','apiUtility',function($scope,$state,apiUtility)
{
	//yaha pe instead of Employee.html if you write our api's ka url , toh api call ho jayega.

	apiUtility.executeApi('GET','http://localhost:8000/alluser')
	.then(function(response){

		$scope.employees=response.data;

	});

	/*
	$http.get("http://localhost:8000/alluser").
	success(function(response)
	{
		
	});
	*/

}]);


app.controller("prodCtrl",['$scope','$state','apiUtility',function($scope,$state,apiUtility)
{
		//var requestData = {};

		//requestData.Usename = '';


	
		apiUtility.executeApi('GET','http://localhost:8000/product')
		.then(function(response){

			$scope.productDetails=response.data;

	});

}]);


app.controller("logoutCtrl",['$scope','$state','apiUtility',function($scope,$state,apiUtility)
{


	$scope.logout=function(){

		apiUtility.executeApi('GET','http://localhost:8000/logout')
		.then(function(response){


			$state.go('login');

		});
}

	//$http.get("http://localhost:8000/product").
	//success(function(response)
	//{
		//$scope.productDetails=response.data;
	//});

}]);




app.service('apiUtility',['$q','$http',function($q,$http){
	

	this.executeApi = function(method, url, requestData, cacheKey, headersObject)
	{


		var deferred=$q.defer();


			$http({
					method: method,
	                    url: url,
	                    headers: headersObject, // it takes object as value
	                    data: requestData,
	                }).success(function(data)
	                  {
	                    deferred.resolve(data);
	                  }).error(function(data, status)
	                 {
	                	deferred.reject({ errorData: data, statusCode: status });

	                 });

	            
		return deferred.promise;
	}	

}]);

app.service('userService', function(){

	this.user = null;

	this.setUser = function(user){

		this.user = user;

	};

	this.getUser = function(){

		return this.user;

	};

});












