<?php

use Illuminate\Database\Seeder;
class BatchTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $users = DB::table('users')->select('id')->get();
       
        DB::table('batch')->truncate();
        
        for($i=1; $i<=10; $i++)
        {       
            DB::table('batch')->insert([            
            'batchcode'           => $faker->numberBetween($min = 1000, $max = 9000),      
            'user_id'             => $faker->randomDigit($users),
            'deliveredperson'     => str_random(10),
            'created_at'          => $faker->dateTime($max = 'now'),
            'updated_at'          => $faker->dateTime($max = 'now')
            ]);
        }
    }
}
