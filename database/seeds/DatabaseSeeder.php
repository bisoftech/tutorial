<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();
         $this->call(UserTableSeeder::class);
         $this->call(CustomerTableSeeder::class);
         $this->call(BatchTableSeeder::class);
         $this->call(BillingTableSeeder::class);
         $this->call(ProductTableSeeder::class);
         $this->call(ProductBillTableSeeder::class);
        Model::reguard();
    }
}
