<?php

use Illuminate\Database\Seeder;
use App\Bill;
use App\Billing;
use App\product;

class ProductBillTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $bill             = Billing::select('id')->get();
        $product_id       = product::select('id')->get();
        
        Bill::truncate();
        for($i=1; $i<10; $i++)
        {
<<<<<<< HEAD
            Bill::create([    
=======
            Bill::create([
>>>>>>> 162dc5313b1dda610febd49f786e4d5198239aa6
            'bill_id'       => $faker->randomDigit($bill),      
            'product_id'    => $faker->randomDigit($product_id), 
            'quantity'      => $faker->numberBetween($min = 1, $max = 100),
            'rate'          => $faker->numberBetween($min = 100, $max = 9000),
            'created_at'    => $faker->dateTime($max = 'now'),
            'updated_at'    => $faker->dateTime($max = 'now')    
             ]);
        }
    }
}
